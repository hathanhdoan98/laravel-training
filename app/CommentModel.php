<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CommentModel extends Model
{
    protected $table            = 'comments';
    protected $fillable = ['comment_content','comment_publish_date','user_id','post_id'];

    public function getItemByCondition($params){
        if($params){
            $query = $this->select('id','comment_content','comment_publish_date','user_id','post_id');
            foreach ($params as $key=>$value){
                $query->where($key,'=',$value);
            }

            $result = $query->get()->toArray();
            return $result;
        }
    }

}
