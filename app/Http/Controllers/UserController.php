<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use Hash;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(Request $request){
        $user = $this->user->create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);

        return response()->json([
            'status'=> 200,
            'message'=> 'User created successfully',
            'data'=>$user
        ]);
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token ='';
        $user = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['invalid_email_or_password'], 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }

        $user = auth()->user();



        return response()->json(['token'=>$token,'user'=>$user],200);
    }

    public function getUserInfo(Request $request){
        //$user = $request->user();
        //$user = JWTAuth::toUser($request->token);
        $request->query('email');
        //$request->get('email');
        $user = auth()->user();
        return response()->json(['result' => $user]);
    }
    public function getPayload(Request $request){
        $payLoad= JWTAuth::getPayload();
        return response()->json(['result' => $payLoad]);
    }
}
