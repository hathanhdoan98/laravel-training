<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\PostModel;
class IndexController extends Controller{

    public function __construct()
    {
    }

    public function index()
    {
        $postModel = new PostModel();
        $items = $postModel->getItemList();

        return view('index.index',['items'=>$items]);
    }
}


?>


