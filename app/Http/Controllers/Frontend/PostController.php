<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\PostModel;
use App\UserModel;
use App\CommentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller{

    public function __construct()
    {
        $this->model = new PostModel();
    }

    public function detail($slug)
    {
        $commentModel = new CommentModel();
        $userModel    = new UserModel();
        $items = $this->model->getItemByCondition(array('slug'=>$slug));
        $postId = $items[0]['id'];
        $comments = $commentModel->getItemByCondition(array('post_id'=>$postId));
        foreach ($comments as $key=>$comment){
            $user = ($userModel->getItemByCondition(array('id'=>$comment['user_id'])))[0];
            $comments[$key]['user'] = $user;
        }

        return view('index.postDetail',['item'=>$items[0],'comments'=>$comments,'postId'=>$postId]);
    }

    public function  addComment(Request $request){
        $result = array();
        if(!Auth::check()) {
            $result['code'] = -1;
            $result['msg'] = "Please Login!";
        }
        else{

            $userId = Auth::user()->id;
            $request['user_id'] = $userId;
            $newComment = CommentModel::create($request->all());
            $result['code'] = $newComment==null?0:1;

            $result['msg'] = $result['code']==1?"Success":"Fail";
        }

        echo json_encode($result);
    }
}


?>


