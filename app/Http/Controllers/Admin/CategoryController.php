<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\CategoryModel;
use App\PostDetailModel;
use App\PostModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->model = new CategoryModel();

    }

    public function index()
    {
        $items = $this->model->getItemList();

//         foreach ($items as $key=>$value){
//             $items[$key]['category_id'] = $postDetailModel->getItemByCondition(array('post_id'=>$value['id']));
//         }

        return view('admin.pages.category.index', ['title' => 'CATEGORY ADMIN', 'items' => $items]);
    }

    public function add(Request $request)
    {


        $target_dir = asset('img') . '/';
        $error = array();
        $error = $this->uploadImg($target_dir, $_FILES['img']);

        if (!empty($error)) {
            return redirect()->route('categoryIndex')->with('error_notify', $error);
        } else {
            $nameFile = $_FILES['img']['name'];

            $data['category_name'] = $request['category_name'];
            $data['category_img'] = $nameFile;
            $data['status'] = $request['category_status'];

            $new = CategoryModel::create($data);

            if ($new) {
                $request->img->move('img', $request->img->getClientOriginalName());
                return redirect()->route('categoryIndex')->with('success_notify', 'Success!');
            } else
                return redirect()->route('categoryIndex')->with('error_notify', array('* Fail!'));
        }

    }

    public function editIndex($id)
    {
        $item = $this->model->getItemByCondition(array('id'=>$id))[0];

        return view('admin.pages.category.edit',['title'=>'EDIT POST','item'=>$item]);

    }

    public function edit(Request $request)
    {
        $id     = $request['id'];
        $target_dir = asset('img') . '/';
        $error = array();
        $error = $this->uploadImg($target_dir, $_FILES['img']);

        if (!empty($error)) {
            return redirect()->route('editIndexCategory',['id'=>$id])->with('error_notify', $error);
        } else {
            $nameFile = $_FILES['img']['name'];
            $oldName  = $request['oldImg'];

            $data['category_img'] = $nameFile;
            $data['category_name'] = $request['category_name'];
            $data['status'] = $request['status'];

            $this->model->updateItem($data,$id);

            // Check if file already exists
            if (file_exists($target_dir.$oldName)) {
                unlink($target_dir.$oldName);
            }
            $request->img->move('img', $request->img->getClientOriginalName());

            return redirect()->route('editIndexCategory',['id'=>$id])->with('success_notify', 'Success!');
        }
    }

    public function delete($id)
    {
        $this->model->deleteItem($id);
        return redirect()->route('categoryIndex')->with('success_notify', 'Success!');
    }

    public function changeStatus($id, $status)
    {
        $this->model->changeStatus($id, $status);
        return redirect()->route('categoryIndex')->with('success_notify', 'Success!');
    }
}
