<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PostModel;
use App\PostDetailModel;
use App\CategoryModel;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Array_;

class PostController extends Controller
{

    public function __construct()
    {
        $this->model = new PostModel();

    }

    public function index()
    {

        $items = $this->model->getItemList();
        $categoryModel = new CategoryModel();
        $postDetailModel = new PostDetailModel();
        $categories = $categoryModel->getItemList();

//         foreach ($items as $key=>$value){
//             $items[$key]['category_id'] = $postDetailModel->getItemByCondition(array('post_id'=>$value['id']));
//         }

        return view('admin.pages.post.index', ['title' => 'POST ADMIN', 'items' => $items, 'categories' => $categories]);
    }

    public function add(Request $request)
    {


        $target_dir = asset('img') . '/';
        $error = array();
        $error = $this->uploadImg($target_dir, $_FILES['img']);

        if (!empty($error)) {
            return redirect()->route('postIndex')->with('error_notify', $error);
        } else {
            $nameFile = $_FILES['img']['name'];
            $publish_date = date("Y/m/d");

            $request['slug'] = Str::slug($request['post_title']);
            $request['post_publish_date'] = $publish_date;
            $data = $request->except('img','category_id');
            $data['post_img'] = $nameFile;
            $data['slug'] = Str::slug($request['post_title']);
            $data['post_publish_date'] = $publish_date;

            $new = PostModel::create($data);

            if ($new) {
                $detail = new PostDetailModel();
                foreach ($request['category_id'] as $categoryId) {
                    $detail->addItem($new['id'], $categoryId);
                }
                $request->img->move('img', $request->img->getClientOriginalName());
                return redirect()->route('postIndex')->with('success_notify', 'Success!');
            } else
                return redirect()->route('postIndex')->with('error_notify', array('* Fail!'));
        }

    }

    public function editIndex($id)
    {
        $item = $this->model->getItemByCondition(array('id'=>$id))[0];
        $categoryModel = new CategoryModel();
        $detailModel = new PostDetailModel();

        $detail= $detailModel->getItemByCondition(array('post_id'=>$id));
        foreach ($detail as $value){
            $item['category_id'][] = $value['category_id'];
        }

        $categories = $categoryModel->getItemList();


        return view('admin.pages.post.edit',['title'=>'EDIT POST','item'=>$item,'categories'=>$categories]);

    }

    public function edit(Request $request)
    {
        $id     = $request['id'];
        $target_dir = asset('img') . '/';
        $error = array();
        $error = $this->uploadImg($target_dir, $_FILES['img']);

        if (!empty($error)) {
            return redirect()->route('editIndexPost',['id'=>$id])->with('error_notify', $error);
        } else {
            $nameFile = $_FILES['img']['name'];
            $oldName  = $request['oldImg'];
            $publish_date = date("Y/m/d");

            $data = $request->except('img','category_id','_token','oldImg');
            $data['post_img'] = $nameFile;
            $data['slug'] = Str::slug($request['post_title']);
            $data['post_publish_date'] = $publish_date;

            $this->model->updateItem($data,$id);

                $detail = new PostDetailModel();
                $detail->deleteItemByCondition(array('post_id'=>$id));
                foreach ($request['category_id'] as $categoryId) {
                    $detail->addItem($id, $categoryId);
                }
                // Check if file already exists
                if (file_exists($target_dir.$oldName)) {
                    unlink($target_dir.$oldName);
                }
                $request->img->move('img', $request->img->getClientOriginalName());

                return redirect()->route('editIndexPost',['id'=>$id])->with('success_notify', 'Success!');
        }
    }

    public function delete($id)
    {
        $this->model->deleteItem($id);
        return redirect()->route('postIndex',['id'=>$id])->with('success_notify', 'Success!');
    }

    public function changeStatus($id,$status)
    {
        $this->model->changeStatus($id,$status);
        return redirect()->route('postIndex',['id'=>$id])->with('success_notify', 'Success!');
    }


}

?>


