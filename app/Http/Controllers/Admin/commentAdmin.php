<?php
include ROOT . DS . 'models' . DS . 'commentModel.php';
class commentAdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    function commentAdmin_index()
    {

        $data = array();
        $model = new PostModel();
        $categoryModel = new CategoryModel();

        $categories = $categoryModel->getCategoryList();
        $items = $model->getPostList();

        $data['items'] = $items;
        $data['categories'] = $categories;
        $data['title'] = 'Post admin';
        $data['template_file'] = 'admin/pages/post/index.php';
        render('admin/layout.php', $data);
    }

    function commentAdmin_add()
    {
            $data = $_GET;
            unset($data['md']);
            unset($data['c']);
            unset($data['m']);
            $commentModel = new CommentModel();
            $result = $commentModel->addComment($data);
            if ($result > 0) {
                echo '<div class="row">
        <div class="col-sm-1">
            <div class="thumbnail">
                <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
            </div>
        </div>
    
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>' . $_SESSION['email'] . '</strong> <span class="text-muted">' . $_GET['post_date'] . '</span>
                </div>
                <div class="panel-body">
                    <p>' . $_GET['content'] . '</p>
                </div>
            </div>
        </div>
    
    </div>';
            } else
                echo false;     
    }

    function commentAdmin_editIndex()
    {

        $data = array();
        $model = new PostModel();
        $categoryModel = new CategoryModel();

        $categories = $categoryModel->getCategoryList();
        $item = $model->getPostById($_GET['id']);

        $data['item'] = $item;
        $data['categories'] = $categories;
        $data['title'] = 'Post Edit';
        $data['template_file'] = 'admin/pages/post/edit.php';
        render('admin/layout.php', $data);
    }

    function commentAdmin_edit()
    {

        $id = $_POST['id'];
        $nameOldImg = $_POST['nameImg'];
        $nameFile = $_FILES['img']['name'];
        $publish_date = date("Y/m/d");

        $target_dir = ROOT . DS . '/public/img/';
        //echo $target_dir.$nam
        // Check if file already exists
        if (file_exists($target_dir . $nameOldImg)) {
            unlink($target_dir . $nameOldImg);
        }

        $error = array();
        $error = uploadImg($target_dir, $_FILES['img']);

        $_POST['img'] = $nameFile;
        $_POST['publish_date'] = $publish_date;

        if (!empty($error)) {
            //unset($_SESSION['notify']);
            $_SESSION['error'] = $error;
            redirect('?md=admin&c=commentAdmin&m=editIndex&id=' . $id);
        } else {
            //unset($_SESSION['error']);
            //$title = $_POST['title']

            array_shift($_POST);
            unset($_POST['nameImg']);
            $model = new PostModel();
            $result = $model->updatePostById($id, $_POST);
            if ($result > 0) {
                $_SESSION['notify'] = 'Success!';
                redirect('?md=admin&c=commentAdmin&m=editIndex&id=' . $id);
            } else {
                $_SESSION['error'][] = 'Fail!';
                redirect('?md=admin&c=commentAdmin&m=editIndex&id=' . $id);
            }
        }
    }

    function commentAdmin_delete()
    {
        $model = new PostModel();
        $result = $model->deletePost($_GET['id']);
        if ($result != null) {
            $_SESSION['notify'] = 'Success!';
            redirect('?md=admin&c=commentAdmin&m=index');
        }
    }

    function commentAdmin_changeStatus()
    {
        
        $id = $_GET['id'];
        $status = $_GET['status'] == 1 ? 0 : 1;

        $feild = array();
        $feild['status'] = $status;

        $model = new PostModel();
        $result = $model->updatePostById($id, $feild);
        if ($result != null) {
            $_SESSION['notify'] = 'Success!';
            redirect('?md=admin&c=commentAdmin&m=index');
        }
    }
}
