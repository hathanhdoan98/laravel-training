<?php
include ROOT . DS . 'models' . DS . 'postModel.php' ;
include ROOT . DS . 'models' . DS . 'categoryModel.php' ;
function userAdmin_index()
{
     
    $data = array();
    $model = new PostModel();
    $categoryModel = new CategoryModel();

    $categories = $categoryModel->getCategoryList();
    $items = $model->getPostList();

    $data['items'] = $items;
    $data['categories'] = $categories;
    $data['title'] = 'Post admin';
    $data['template_file'] = 'admin/pages/post/index.php';
    render('admin/layout.php', $data);
}

function userAdmin_add(){
     

    $nameFile = $_FILES['img']['name'];   
    $publish_date = date("Y/m/d");

    $target_dir = ROOT . DS .'/public/img/';
    $error= array();
    $error = uploadImg($target_dir,$_FILES['img']);

    $_POST['img'] = $nameFile;
    $_POST['publish_date']= $publish_date;
    
    if(!empty($error)){
        //unset($_SESSION['notify']);
        $_SESSION['error'] = $error;
        redirect('?md=admin&c=userAdmin&m=index');
    }else{

        $model = new PostModel();
        $result = $model->addPost($_POST);
        if($result!=null){
            $_SESSION['notify'] = 'Success!';
            redirect('?md=admin&c=userAdmin&m=index');
        }
    } 
}

function userAdmin_editIndex()
{
     

    $data = array();
    $model = new PostModel();
    $categoryModel = new CategoryModel();

    $categories = $categoryModel->getCategoryList();
    $item= $model->getPostById($_GET['id']);

    $data['item'] = $item;
    $data['categories'] = $categories;
    $data['title'] = 'Post Edit';
    $data['template_file'] = 'admin/pages/post/edit.php';
    render('admin/layout.php', $data);
}

function userAdmin_edit(){

    $id = $_POST['id'];
    $nameOldImg = $_POST['nameImg'];
    $nameFile = $_FILES['img']['name'];   
    $publish_date = date("Y/m/d");

    $target_dir = ROOT . DS .'/public/img/';
    //echo $target_dir.$nam
// Check if file already exists
    if (file_exists($target_dir.$nameOldImg)) {
        unlink($target_dir.$nameOldImg);
    }

    $error= array();
    $error = uploadImg($target_dir,$_FILES['img']);

    $_POST['img'] = $nameFile;
    $_POST['publish_date']= $publish_date;
    
    if(!empty($error)){
        //unset($_SESSION['notify']);
        $_SESSION['error'] = $error;
        redirect('?md=admin&c=userAdmin&m=editIndex&id='.$id);
    }else{
        //unset($_SESSION['error']);
        //$title = $_POST['title']
        
        array_shift($_POST);
        unset($_POST['nameImg']);
        $model = new PostModel();
        $result = $model->updatePostById($id,$_POST);
        if($result>0){
            $_SESSION['notify'] = 'Success!';
            redirect('?md=admin&c=userAdmin&m=editIndex&id='.$id);
        }else{
            $_SESSION['error'][] = 'Fail!';
            redirect('?md=admin&c=userAdmin&m=editIndex&id='.$id);
        }
    } 
}

function userAdmin_delete()
{
     
    $model = new PostModel();
    $result = $model->deletePost($_GET['id']);
    if($result!=null){
        $_SESSION['notify'] = 'Success!';
        redirect('?md=admin&c=userAdmin&m=index');
    }
}

function userAdmin_changeStatus()
{
    
    $id = $_GET['id'];
    $status = $_GET['status']==1?0:1;

    $feild = array();
    $feild['status'] = $status;

    $model = new PostModel();
    $result = $model->updatePostById($id,$feild);
    if($result!=null){
        $_SESSION['notify'] = 'Success!';
        redirect('?md=admin&c=userAdmin&m=index');
    }
}

?>


