<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\CommentModel;
use Illuminate\Http\Request;


class CommentController extends Controller{

    public function __construct()
    {
    }

    public function index()
    {
        return CommentModel::all();
    }


    public function store(Request $request)
    {
        $comment = CommentModel::create($request->all());
        return response()->json($comment, 201);
    }
}


?>


