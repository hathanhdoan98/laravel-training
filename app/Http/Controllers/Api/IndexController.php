<?php
namespace App\Http\Controllers\Api;
use App\CommentModel;
use App\Http\Controllers\Controller;
use App\PostModel;
use App\UserModel;


class IndexController extends Controller{

    public function __construct()
    {
        $this->model = new  PostModel();
    }

    public function index()
    {
        return PostModel::all();
    }

    public function detail($slug)
    {
        $commentModel = new CommentModel();
        $userModel    = new UserModel();
        $items = $this->model->getItemByCondition(array('slug'=>$slug));
        $postId = $items[0]['id'];
        $comments = $commentModel->getItemByCondition(array('post_id'=>$postId));
        foreach ($comments as $key=>$comment){
            $user = ($userModel->getItemByCondition(array('id'=>$comment['user_id'])))[0];
            $comments[$key]['user_email'] = $user['email'];
        }

        return response()->json(['item'=>$items[0],'comments'=>$comments],200);
    }
}


?>


