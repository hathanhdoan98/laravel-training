<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected  $model;

    function uploadImg($target_dir, $file)
    {
        $error= array();
        $target_file = $target_dir . basename($file["name"]);
        //$target_file = $_SERVER['DOCUMENT_ROOT'].'/'.basename($file["name"]);
        //exit();

        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if ($check !== false) {
                echo "* File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                $error[]= "* File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        // if (file_exists($target_file)) {
        //     $error[]= "* File already exists.";
        //     $uploadOk = 0;
        // }
        // Check file size
        if ($file["size"] > 500000) {
            $error[]= "* Your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            $error[]= "* Only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $error[]= "* Your file was not uploaded.";
            // if everything is ok, try to upload file
        }
        return $error;
    }
}
