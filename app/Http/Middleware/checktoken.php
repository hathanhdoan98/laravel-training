<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class checktoken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if ($user) {
                return $next($request);
            } else {
                return response()->json([
                        'error' => 'Authorization Token not found',
                        'message' => 'Authorization Token not found',
                        'status' => 401
                    ]
                    , 401);
            }
        } catch (\Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['status' => 401, 'message' => 'Token is Invalid'], 401);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['status' => 401, 'message' => 'Token is Expired'], 401);
            } else {
                return response()->json(['status' => 401, 'message' => 'Authorization Token not found'], 401);
            }
        }
    }
}
