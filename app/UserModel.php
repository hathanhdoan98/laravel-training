<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table            = 'users';
    public function getItemByCondition($params){
        if($params){
            $query = $this->select('id','name','email');
            foreach ($params as $key=>$value){
                $query->where($key,'=',$value);
            }

            $result = $query->get()->toArray();
            return $result;
        }
    }
}
