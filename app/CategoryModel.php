<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PostDetailModel;

class CategoryModel extends Model
{
    protected $table            = 'categories';
    protected $fillable = ['category_name','category_img','status'];

    public function getItemList(){
        $result = self::select('id','category_name','status','category_img');
        return $result->get()->toArray();

    }
    public function getItemByCondition($params){
        if($params){
            $query = $this->select('id','category_name','category_img','status');
            foreach ($params as $key=>$value){
                $query->where($key,'=',$value);
            }

            $result = $query->get()->toArray();
            return $result;
        }
    }
    public function changeStatus($idCategory,$status){
        $status = $status==0?1:0;
        self::where('id',$idCategory)->update(['status'=>$status]);
    }
    public function  deleteItem($id){
        $postDetail = new PostDetailModel();
        $postDetail::where('category_id',$id)->delete();
        self::where('id',$id)->delete();

    }
    public function updateItem($updateParram,$id){
        self::where('id',$id)->update($updateParram);
    }
}
