<?php

namespace App;
use App\PostDetailModel;

use Illuminate\Database\Eloquent\Model;

class PostModel extends Model
{
    protected $table            = 'posts';

    protected $fillable = ['post_title','post_img','post_publish_date','status','category_id','slug','post_content'];

    public $timestamps = false;

    public function getItemList(){

        $result = self::select('id','post_title','post_img','status','post_content','post_publish_date','slug');
        return $result->get()->toArray();
    }
    public function getItemByCondition($params){
        if($params){
            $query = $this->select('id','post_title','post_content','status','post_img','post_publish_date','slug');
            foreach ($params as $key=>$value){
                $query->where($key,'=',$value);
            }

        $result = $query->get()->toArray();
        return $result;
        }
    }

    public function  deleteItem($id){
        $postDetail = new PostDetailModel();
        $postDetail::where('post_id',$id)->delete();
        self::where('id',$id)->delete();

    }

    public function changeStatus($idPost,$status){
        $status = $status==0?1:0;
        self::where('id',$idPost)->update(['status'=>$status]);
    }

    public function updateItem($updateParram,$id){
        self::where('id',$id)->update($updateParram);
    }
}
