<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostDetailModel extends Model
{
    protected $table            = 'post_detail';
    public function getItemByCondition($params){
        if($params){
            $query = $this->select('post_id','category_id');
            foreach ($params as $key=>$value){
                $query->where($key,'=',$value);
            }

            $result = $query->get()->toArray();
            return $result;
        }
    }

    public function addItem($postId, $categoryId){
        $detail = new PostDetailModel();
        $detail->post_id = $postId;
        $detail->category_id = $categoryId;

        $detail->save();
    }
    public function deleteItemByCondition($whereParam){
        $detail =self::where('post_id','<>','-2');
        foreach ($whereParam as $key=>$value){
            $detail->where($key,$value);
        }

        $detail->delete();
    }
}
