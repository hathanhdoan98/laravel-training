var App = {
    getCurrentDateByFormat:function () {
        var currentDate = new Date();

        var dd = currentDate.getDate();
        var mm = currentDate.getMonth() + 1;

        var yyyy = currentDate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var currentDate = yyyy + '/' + mm + '/' + dd;
        return currentDate;
    },
    loadPosts:function () {
            $.ajax({
                type: 'GET',
                url: '/api/post',
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status == 200) {

                        $.each(data,function (key,value) {
                            $('#postList').append('<div class="post-preview">\n' +
                                '    <a href="/frontendapi/detail?slug='+ value.slug +'">\n' +
                                '     <h2 class="post-title">'+ value.post_title+'\n' +
                                '      </h2>\n' +
                                '     <image src="/img/'+ value.post_img +'"></image>\n' +
                                '      </a>\n' +
                                '    <p class="post-meta">Posted '+ value.post_publish_date +'\n' +
                                '    </p>\n' +
                                ' </div>\n' +
                                '<hr>');
                        });
                        // localStorage.setItem("user", JSON.stringify(data.user));
                        // localStorage.setItem("token", JSON.stringify(data.token));
                        //console.log(JSON.parse(localStorage.getItem('user')));
                        // window.location='frontendapi/index';
                    }
                },
                error: function (xhr) { // if error occured
                    alert(xhr.statusText + xhr.responseText);
                },
                dataType: 'json'
            });
    },
    loadDetail:function (slug) {
        var user_email = JSON.parse(localStorage.getItem('user')).email;
        $('#user_email').html(user_email);

        $.ajax({
            type: 'GET',
            url: '/api/detail/'+slug,

            success: function (data, textStatus, jqXHR) {

                if (jqXHR.status == 200) {

                    $.each(data.comments,function (key,value) {

                        $('#commentList').append('<div class="row">\n' +
                            '    <div class="col-sm-1">\n' +
                            '        <div class="thumbnail">\n' +
                            '            <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">\n' +
                            '        </div>\n' +
                            '    </div>\n' +
                            '\n' +
                            '    <div class="col-sm-8">\n' +
                            '        <div class="panel panel-default">\n' +
                            '            <div class="panel-heading">\n' +
                            '                <strong>'+ value.user_email+'</strong> <span class="text-muted">'+ value.comment_publish_date+'</span>\n' +
                            '            </div>\n' +
                            '            <div class="panel-body">\n' +
                            '                <p>'+ value.comment_content +'</p>\n' +
                            '            </div>\n' +
                            '        </div>\n' +
                            '    </div>\n' +
                            '\n' +
                            '</div>');
                    });

                    $('#post_detail').append('<h2 class="section-heading">'+ data.item.post_title +'</h2>\';\n' +
                        '<image src="/img/'+ data.item.post_img +'"></image>\';\n' +
                        '<h3>'+ data.item.post_content+'</h3>;');
                    $('#contentComment').attr('data-postid', data.item.id);
                    // localStorage.setItem("user", JSON.stringify(data.user));
                    // localStorage.setItem("token", JSON.stringify(data.token));
                    //console.log(JSON.parse(localStorage.getItem('user')));
                    // window.location='frontendapi/index';
                }
            },
            error: function (xhr) { // if error occured
                alert(xhr.statusText + xhr.responseText);
            },
            dataType: 'json'
        });
    },
    checkLogin:function () {
        var user = JSON.parse(localStorage.getItem('user'));
        if(user){
            window.location='frontendapi/index';
        }
    },
    login:function () {
        $('#btnLogin').click(function () {
            var email = $('#email').val();
            var password = $('#password').val();

            $.ajax({
                headers: {
                    //'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: 'api/auth/login',
                data: {email: email, password: password},
                success: function (data,textStatus, jqXHR) {
                    if(jqXHR.status==200){
                        localStorage.setItem("user", JSON.stringify(data.user));
                        localStorage.setItem("token", JSON.stringify(data.token));
                        window.location='frontendapi/index';
                    }
                },
                error: function (xhr) { // if error occured
                    //alert(xhr.statusText + xhr.responseText);
                    if(xhr.status==422){
                        alert('Email or password is invalid!');
                    }
                },
                dataType: 'json'
            });

        });
    },
    comment:function () {
        $('#btnComment').click(function () {
            var postId=$('#contentComment').attr('data-postid');
            var userId= JSON.parse(localStorage.getItem('user')).id;
            var userEmail= JSON.parse(localStorage.getItem('user')).email;
            var content = $('#contentComment').val();
            var commentDate = App.getCurrentDateByFormat();

            var token = 'Bearer '+ localStorage.getItem('token').replace(/['"]+/g, '');
            console.log(token);

            if(!content.length){
                alert('Please enter content!');
                return;
            }else {

                $.ajax({
                    headers: {
                        'Authorization' : token
                    },
                    type: 'POST',
                    url: '/api/comment',
                    data: {comment_content: content, comment_publish_date: commentDate, post_id: postId, user_id: userId},
                    beforeSend: function () {
                        $('#modal').show();

                    },
                    success: function (data, textStatus, jqXHR) {

                            if (jqXHR.status==201){
                                $('#modal').hide();
                                var newComment = '<div class="row">\n' +
                                    '            <div class="col-sm-1">\n' +
                                    '                <div class="thumbnail">\n' +
                                    '                    <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">\n' +
                                    '                </div>\n' +
                                    '            </div>\n' +
                                    '\n' +
                                    '            <div class="col-sm-8">\n' +
                                    '                <div class="panel panel-default">\n' +
                                    '                    <div class="panel-heading">\n' +
                                    '                        <strong>' + userEmail + '</strong> <span class="text-muted">' + commentDate +
                                    '                    </div>\n' +
                                    '                    <div class="panel-body">\n' +
                                    '                        <p>' + content +
                                    '                    </div>\n' +
                                    '                </div>\n' +
                                    '            </div>\n' +
                                    '\n' +
                                    '        </div>';
                                $('#commentList').append(newComment);
                                $('#contentComment').val('');
                            }else {
                                $('#modal').hide();
                                alert('Please login!');
                            }

                    },
                    error: function (xhr) { // if error occured
                        alert(xhr.statusText + xhr.responseText);
                    },
                    dataType: 'json'
                });
            }
        });
    },
    logout:function () {
        var user = JSON.parse(localStorage.getItem('user'));
        if(user){
            localStorage.removeItem('user');
            window.location='/frontendapi/index';
        }
    }
};


$(document).ready(function(){

});
