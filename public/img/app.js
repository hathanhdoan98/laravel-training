var App = {
    init: function () {
        console.log('hello word');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajaxSetup({
            cache: false,
            beforeSend: function () {
                $('button, button.btn, .btn').addClass('btn-process');
            },
            complete: function () {
                $('button, button.btn, .btn').removeClass('btn-process');
            }
        });
    },
    handleError: function (err) {
        App.stopLoading();
        if (err.status !== 500) {
            let configKey = ['phone', 'password', 'link', 'quantity'];
            let errJson = err.responseJSON.error;
            if (typeof errJson !== "string") {
                let show = 0;
                configKey.forEach(function (item, index) {
                    if (errJson[item]) {
                        let errorMessage = errJson[item].reduce(function (totalMess, indexMess) {
                            return totalMess + ' - ' + indexMess;
                        });
                        $.toast({
                            heading: 'Lỗi dữ liệu của bạn !',
                            text: errorMessage,
                            icon: 'error',
                            loader: true,
                            loaderBg: '#9EC600',
                            position: 'top-right'
                        });
                        show = 1;
                    }
                });
                if (err.responseJSON.message && show === 0) {
                    $.toast({
                        heading: 'Thông báo',
                        text: err.responseJSON.message,
                        icon: 'error',
                        loader: true,
                        loaderBg: '#9EC600',
                        position: 'top-right'
                    });
                }
            } else {
                console.log(err.responseJSON.message);
                if (err.responseJSON.message) {
                    $.toast({
                        heading: 'Thông báo',
                        text: err.responseJSON.message,
                        icon: 'error',
                        loader: true,
                        loaderBg: '#9EC600',
                        position: 'top-right'
                    });
                }
            }

        }
        if (err.status === 500) {
            $.toast({
                heading: 'Thông báo',
                text: 'Server xảy ra sự cố vui lòng thông báo cho admin biết việc này !',
                icon: 'error',
                loader: true,
                loaderBg: '#9EC600',
                position: 'top-right'
            });
        }
        return true;
    },
    loading: function (message) {
        $('body').loading({
            message: message
        });
    },
    stopLoading: function () {
        $('body').loading('stop');
    },
    showErrorNotify: function (message) {
        App.stopLoading();
        $.toast({
            heading: 'Thông báo',
            text: message,
            icon: 'error',
            loader: true,
            loaderBg: '#9EC600',
            position: 'top-right'
        });
        return true;
    },
    showSuccessNotify: function (message) {
        App.stopLoading();
        $.toast({
            heading: 'Thông báo',
            text: message,
            icon: 'success',
            loader: true,
            loaderBg: '#9EC600',
            position: 'top-right'
        });
        return true;
    },
    login: function () {
        $('#btnLogin').click(function () {
            let Phone = $('#phoneLogin').val();
            let Ps = $('#psLogin').val();
            if (!Ps || !Phone) {
                App.showErrorNotify('Vui lòng nhập đầy đủ sdt/mật khẩu để đăng nhập');
                return false;
            } else {
                App.loading('Đang đăng nhập...');
                $.ajax({
                    url: '/login',
                    data: {phone: Phone, password: Ps},
                    type: 'POST',
                    success: function (data) {
                        App.stopLoading();
                        if (data.status == 200) {
                            window.location = '/home';
                        }
                        return false;
                    },
                    error: function (er) {
                        App.showErrorNotify('Vui lòng kiểm tra lại thông tin tài khoản mật khẩu của bạn');
                        App.stopLoading();
                    }
                });
            }
        });
    },
    register: function () {
        $('#btnRegister').click(function () {
            let Phone = $('#phoneLogin').val();
            let Ps = $('#psLogin').val();
            let rePs = $('#rePassLogin').val();
            if (rePs != Ps) {
                App.showErrorNotify('Mật khẩu và xác nhận mật khẩu không giống nhau !');
                return true;
            }
            if (!Ps || !Phone) {
                App.showErrorNotify('Vui lòng nhập đầy đủ sdt/mật khẩu để đăng kí !');
                return true;
            }
            App.loading('Đang đăng kí...');
            setTimeout(function () {
                $.ajax({
                    url: 'register',
                    data: {phone: Phone, password: Ps},
                    type: 'POST',
                    success: function (data) {
                        App.stopLoading();
                        App.showSuccessNotify('Đăng kí thành công vui lòng đăng nhập để sử dụng dịch vụ');
                        setTimeout(2000);
                        window.location = '/dang-nhap';
                    },
                    error: function (er) {
                        App.stopLoading();
                        App.handleError(er);
                    }
                });
            }, 5000);
        });
    }
};
jQuery(document).ready(function ($) {
    App.init();
    App.login();
    App.register();
    App.bookmark();
});
