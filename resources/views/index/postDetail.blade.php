@extends('index.layout')

@section('content')
<?php
use Illuminate\Support\Facades\Auth;
?>
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto" id="post_detail">
<!--                --><?php
//                echo '<h2 class="section-heading">' . $item['post_title'] . '</h2>';
//                echo  '<image src="'.asset('img'). '/' . $item['post_img'] . '"></image>';
//                echo  '<h3>' . $item['post_content'] . '</h3>';
//                ?>

            </div>
        </div>
    </div>
</article>


<div class="row">
    <div class="col-sm-6">
        <h3>User Comment</h3>
    </div><!-- /col-sm-12 -->
</div><!-- /row -->
<div class="container" id ="commentList">
<!-- List comment -->
<?php
//    echo "<pre>";
//     print_r($comments);
//    echo "</pre>";
//    exit();
//    if($comments!=null){
//        foreach ($comments as $value){
//
//            echo '<div class="row">
//            <div class="col-sm-1">
//                <div class="thumbnail">
//                    <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
//                </div>
//            </div>
//
//            <div class="col-sm-8">
//                <div class="panel panel-default">
//                    <div class="panel-heading">
//                        <strong>'.$value['user']['email'].'</strong> <span class="text-muted">'.$value['comment_publish_date'].'</span>
//                    </div>
//                    <div class="panel-body">
//                        <p>'.$value['comment_content'].'</p>
//                    </div>
//                </div>
//            </div>
//
//        </div>';
//        }
//    }
?>
</div>
<!--comment -->


<!------ Include the above in your HEAD tag ---------->

<!-- Your comment -->
<?php
//    $emailUser = isset(Auth::user()->email)?Auth::user()->email:'';
//    $postDate= new DateTime();
//    $dateFormat = $postDate->format("Y-m-d");
//    echo $dateFormat;
//    $userId = isset(Auth::user()->id)?Auth::user()->id:false;
?>

<div class="row">
    <div class="col-sm-1">
        <div class="thumbnail">
            <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
        </div><!-- /thumbnail -->
    </div><!-- /col-sm-1 -->

    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong id="user_email"></strong> <span class="text-muted"></span>
            </div>
            <div class="panel-body">
                <input type="text" id="contentComment" data-postid="" class="form-control" placeholder="text your comment">
            </div><!-- /panel-body -->
            <button type="button" id="btnComment"
            style="margin-left: 10px;margin-bottom: 10px;" class="btn btn-primary">Post</button>
        </div><!-- /panel panel-default -->
    </div><!-- /col-sm-5 -->

</div><!-- /row -->

<script>
    var slug = location.search.split('slug=')[1];
    App.loadDetail(slug);
    App.comment();
</script>
@endsection


