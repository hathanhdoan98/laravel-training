<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login</title>
    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/mystyle.css')}}" rel="stylesheet">
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <!-- Custom fonts for this template -->
    <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="{{asset('css/clean-blog.min.css')}}" rel="stylesheet">
    <!-- <link rel="shortcut icon" href="public/img/a.ico" type="image/x-icon"> -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

</head>
<body style="background-image: url('{{asset('img/bg-01.jpg')}}');">
<!-- Main Content -->
<div class="card" styl style="margin: 50px; width: 50%;box-shadow: 2px 7px #888888; border-radius: 15px">
    <h1 style="margin-top: 30px;" class="card-title">LOGIN</h1>
    Email<input style="margin-top: 30px;" type="email" id="email" class="form-control">
    Password<input style="margin-top: 30px;" type="password" class="form-control" id="password">
    <button id="btnLogin" class="btn btn-primary" style="width:70px ;margin-top: 20px">Login</button>

</div>
<div id="modal"></div>
<!-- Bootstrap core JavaScript -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('js/clean-blog.min.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script>
    App.checkLogin();
    App.login();
</script>

</body>
</html>
