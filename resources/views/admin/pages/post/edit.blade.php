@extends('admin.layout')
@section('content')

<!--    --><?php
//    echo "<pre>";
//     print_r($item);
//    echo "</pre>";
//    exit();
//    ?>
    @if (session('success_notify'))
    <div style="background-color:#33F0FF; width:50%; padding: 10px; border-radius: 10px;">
        <i style="float:right" class="close far fa-times-circle"></i>
        <p>{{session('success_notify')}}</p>
    </div>

@endif
@if (session('error_notify'))
    <div style="background-color:#FF4233; width:50%; padding: 10px; border-radius: 10px;">
        <i style="float:right" class="close far fa-times-circle"></i>
        @foreach (session('error_notify') as $value)
            <p>{{$value}}</p>
        @endforeach
    </div>

@endif
 <?php
//echo $item['content'];
     $categoryXhtml = '<select style="width: 30%" class="form-control" multiple name="category_id[]">';
     if(!empty($categories)){
       foreach($categories as $value){
           if(in_array($value['id'],$item['category_id'])){
                $categoryXhtml.='<option selected value='.$value['id'].'>'.$value['category_name'].'</option>';
           }else{
                $categoryXhtml.='<option value='.$value['id'].'>'.$value['category_name'].'</option>';
           }
       }
    }
       $categoryXhtml.= '</select>';

    if($item['status']==0){
        $statusXhtml = '<option selected value="0">Unactive</option>';
        $statusXhtml .= '<option value="1">active</option>';
    }else{
        $statusXhtml = '<option  value="0">Unactive</option>';
        $statusXhtml .= '<option  selected value="1">active</option>';
    }
?>

<form method="POST" action="{{route('editPost')}}" enctype="multipart/form-data">
    @csrf
        <strong>ID:</strong><input type="text" readonly value="<?php echo $item['id'] ?>"
        style="width: 50%" class="form-control" name="id">
        <strong>Title</strong><input type="text" value="<?php echo $item['post_title'] ?>"
         required style="width: 50%" class="form-control" name="post_title">
        <strong>Image:</strong><input type="file" required style="width: 30%" class="form-control" name="img">
        <image style="width:55px;height:50px" src="{{asset('img')}}/{{$item['post_img']}}"></image> <br>
        <input type="text" hidden name="oldImg" value="<?php echo $item['post_img'];?>">
        <strong>Status:</strong><select style="width: 30%" class="form-control" name="status">
                            <?php echo $statusXhtml;?>

                            </select>
        <strong>Category: </strong><?php echo $categoryXhtml; ?>
        <strong>Content</strong><textarea  required value=""
         style="width: 70%" class="form-control" name="post_content">{{$item['post_content']}}</textarea>
        <button style="margin-top: 10px" type="submit" class="btn btn-primary mb-2">Submit</button>
    </form>
@endsection
