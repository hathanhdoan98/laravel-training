@extends('admin.layout')
@section('content')
    @if (session('success_notify'))
        <div style="background-color:#33F0FF; width:50%; padding: 10px; border-radius: 10px;">
            <i style="float:right" class="close far fa-times-circle"></i>
                <p>{{session('success_notify')}}</p>
        </div>
    @endif
@if (session('error_notify'))
    <div style="background-color:#FF4233; width:50%; padding: 10px; border-radius: 10px;">
        <i style="float:right" class="close far fa-times-circle"></i>
        @foreach (session('error_notify') as $value)
            <p>{{$value}}</p>
        @endforeach
    </div>

@endif

<?php

    $xhtml = '';
    if(!empty($items)){
        foreach($items as $value){
          //$src = 'src="\public\img\' .$value;
            $img = '<image style="width:35px;height:30px" src="'.asset('img').'/'.$value['post_img'].'"></image>';
            //$status = $value['status']==0?'unactive':'active';
            if($value['status']==1){
              $status = '<button type="button" class="btn btn-primary">
              <a style="color:white" href="'.route('changeStatus',['id'=>$value['id'],'status'=>1]).'">active</a></button>';
            }else{
              $status = '<button type="button" class="btn btn-danger">
              <a style="color:white" href="'.route('changeStatus',['id'=>$value['id'],'status'=>0]).'">Unactive</a></button>';
            }
            $xhtml.= '<tr>';
            $xhtml.=      '<th scope="row">'.$value['id'].'</th>';
            $xhtml.=      '<th>'.$value['post_title'].'</th>';
            $xhtml.=      '<td>'.$img.'</td>';
            $xhtml.=       '<td>'.$value['post_content'].'</td>';
            $xhtml.=      '<td>'.$value['post_publish_date'].'</td>';
            $xhtml.=      '<td>'.$status.'</td>';
            $xhtml.=      '<td><a href="'.route('editIndexPost',['id'=>$value['id']]).'"><i class="fas fa-edit"></i></a>.</td>';
            $xhtml.=      '<td><a href="'.route('deletePost',['id'=>$value['id']]).'"><i class="far fa-trash-alt"></i></td>';
            $xhtml.=      '</tr>';
        }
    }

    $categoryXhtml = '<select multiple class="form-control col-md-6" name="category_id[]">';
    if(!empty($categories)){
      foreach($categories as $value){

          $categoryXhtml.='<option value='.$value['id'].'>'.$value['category_name'].'</option>';
      }
      $categoryXhtml.= '</select>';
  }

?>

    <form method="POST" action="{{route('addPost')}}" enctype="multipart/form-data">
        @csrf
        <strong>Title</strong><input type="text" required style="width: 50%" class="form-control" name="post_title">
        <strong>Image:</strong><input type="file" required style="width: 30%" class="form-control" name="img">
        <strong>Status:</strong><select style="width: 30%" class="form-control" name="status">
                            <option value="0">Unactive</option>
                            <option value="1">Active</option>
                          </select>
        <strong>Category: </strong>{!! $categoryXhtml !!}
        <strong>Content</strong><textarea  required style="width: 70%" class="form-control" name="post_content"></textarea>
        <button style="margin-top: 10px" type="submit" class="btn btn-primary mb-2">Submit</button>
    </form>


<table class="table" style="margin-top: 50px">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Title</th>
      <th scope="col">Image</th>
      <th scope="col">Content</th>
      <th scope="col">Publish Date</th>
      <th scope="col">Status</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    {!! $xhtml !!}
  </tbody>
</table>
@endsection

