@extends('admin.layout')
@section('content')
    @if (session('success_notify'))
        <div style="background-color:#33F0FF; width:50%; padding: 10px; border-radius: 10px;">
            <i style="float:right" class="close far fa-times-circle"></i>
            <p>{{session('success_notify')}}</p>
        </div>
    @endif
    @if (session('error_notify'))
        <div style="background-color:#FF4233; width:50%; padding: 10px; border-radius: 10px;">
            <i style="float:right" class="close far fa-times-circle"></i>
            @foreach (session('error_notify') as $value)
                <p>{{$value}}</p>
            @endforeach
        </div>

    @endif

<?php
//    echo "<pre>";
//     print_r($items);
//    echo "</pre>";
    //exit();
    $xhtml = '';
    if(!empty($items)){
        foreach($items as $value){
          //$src = 'src="\public\img\' .$value;
            $img = '<image style="width:35px;height:30px" src="'.asset('img').'/'.$value['category_img'].'"></image>';
            //$status = $value['status']==0?'unactive':'active';
            if($value['status']==1){
                $status = '<button type="button" class="btn btn-primary">
              <a style="color:white" href="'.route('changeCategoryStatus',['id'=>$value['id'],'status'=>1]).'">active</a></button>';
            }else{
                $status = '<button type="button" class="btn btn-danger">
              <a style="color:white" href="'.route('changeCategoryStatus',['id'=>$value['id'],'status'=>0]).'">Unactive</a></button>';
            }
            $xhtml.= '<tr>';
            $xhtml.=      '<th scope="row">'.$value['id'].'</th>';
            $xhtml.=      '<th>'.$value['category_name'].'</th>';
            $xhtml.=      '<td>'.$img.'</td>';
            $xhtml.=      '<td>'.$status.'</td>';
            $xhtml.=      '<td><a href="'.route('editIndexCategory',['id'=>$value['id']]).'"><i class="fas fa-edit"></i></a>.</td>';
            $xhtml.=      '<td><a href="'.route('deleteCategory',['id'=>$value['id']]).'"><i class="far fa-trash-alt"></i></td>';
            $xhtml.=      '</tr>';
        }
    }

?>

    <form method="POST" action="{{route('addCategory')}}" enctype="multipart/form-data">
        @csrf
        <strong>Name</strong><input type="text" required style="width: 50%" class="form-control" name="category_name">
        <strong>Image:</strong><input type="file" required style="width: 30%" class="form-control" name="img">
        <strong>Status:</strong><select style="width: 30%" class="form-control" name="category_status">
                            <option value="0">Unactive</option>
                            <option value="1">Active</option>
                          </select>

        <button style="margin-top: 10px" type="submit" class="btn btn-primary mb-2">Submit</button>
    </form>


<table class="table" style="margin-top: 50px">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Image</th>
      <th scope="col">Status</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $xhtml; ?>
  </tbody>
</table>
@endsection
