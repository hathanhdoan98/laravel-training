@extends('admin.layout')
@section('content')

    <!--    --><?php
    //    echo "<pre>";
    //     print_r($item);
    //    echo "</pre>";
    //    exit();
    //    ?>
    @if (session('success_notify'))
        <div style="background-color:#33F0FF; width:50%; padding: 10px; border-radius: 10px;">
            <i style="float:right" class="close far fa-times-circle"></i>
            <p>{{session('success_notify')}}</p>
        </div>

    @endif
    @if (session('error_notify'))
        <div style="background-color:#FF4233; width:50%; padding: 10px; border-radius: 10px;">
            <i style="float:right" class="close far fa-times-circle"></i>
            @foreach (session('error_notify') as $value)
                <p>{{$value}}</p>
            @endforeach
        </div>

    @endif
    <?php

    if($item['status']==0){
        $statusXhtml = '<option selected value="0">Unactive</option>';
        $statusXhtml .= '<option value="1">active</option>';
    }else{
        $statusXhtml = '<option  value="0">Unactive</option>';
        $statusXhtml .= '<option  selected value="1">active</option>';
    }
?>

<form method="POST" action="{{route('editCategory')}}" enctype="multipart/form-data">
    @csrf
        <strong>ID:</strong><input type="text" readonly value="<?php echo $item['id'] ?>"
        style="width: 50%" class="form-control" name="id">
        <strong>Name</strong><input type="text" value="<?php echo $item['category_name'] ?>"
         required style="width: 50%" class="form-control" name="category_name">
        <strong>Image:</strong><input type="file" required style="width: 30%" class="form-control" name="img">
        <image style="width:55px;height:50px" src="{{asset('img')}}/{{$item['category_img']}}"></image> <br>
        <input type="text" hidden name="oldImg" value="<?php echo $item['category_img'];?>">
        <strong>Status:</strong><select style="width: 30%" class="form-control" name="status">
                            <?php echo $statusXhtml;?>

                            </select>
        <button style="margin-top: 10px" type="submit" class="btn btn-primary mb-2">Submit</button>
    </form>
@endsection
