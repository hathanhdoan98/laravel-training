<?php
   // print_r($items);
   if(isset($_SESSION['error'])){
     $error = '<div style="background-color:#FF4233; width:50%; padding: 10px; border-radius: 10px;">';
     $error .= '<i style="float:right" class="close far fa-times-circle"></i>';
     foreach($_SESSION['error'] as $value){
        $error.='<p>'.$value.'</p>';
     }
     $error.= '</div>';
     echo $error;
     unset($_SESSION['error']);
   }
   if(isset($_SESSION['notify'])){
    $notify = '<div style="background-color:#33F0FF; width:50%; padding: 10px;border-radius: 10px;">';
    $notify .= '<i style="float:right" class="close far fa-times-circle"></i>';
    $notify .= '<p style="color:white">'.$_SESSION['notify'].'</p>';
    $notify.= '</div>';
    echo $notify;
    unset($_SESSION['notify']);
  }

    $xhtml = '';
    if(!empty($items)){
        foreach($items as $value){
          //$src = 'src="\public\img\' .$value;
            $img = '<image style="width:35px;height:30px" src="/public/img/'.$value['img'].'"></image>';
            //$status = $value['status']==0?'unactive':'active';
            if($value['status']==1){
              $status = '<button type="button" class="btn btn-primary">
              <a style="color:white" href="?md=admin&c=postAdmin&m=changeStatus&id='.$value['id'].'&status=1">active</a></button>';
            }else{
              $status = '<button type="button" class="btn btn-danger">
              <a style="color:white" href="?md=admin&c=postAdmin&m=changeStatus&id='.$value['id'].'&status=0">unactive</button>';
            }
            $xhtml.= '<tr>';
            $xhtml.=      '<th scope="row">'.$value['id'].'</th>';
            $xhtml.=      '<th>'.$value['title'].'</th>';
            $xhtml.=      '<td>'.$img.'</td>';
            $xhtml.=       '<td>'.$value['content'].'</td>';
            $xhtml.=      '<td>'.$value['publish_date'].'</td>';
            $xhtml.=      '<td>'.$status.'</td>';
            $xhtml.=      '<td><a href="?md=admin&c=postAdmin&m=editIndex&id='.$value['id'].'"><i class="fas fa-edit"></i></a>.</td>';
            $xhtml.=      '<td><a href="?md=admin&c=postAdmin&m=delete&id='.$value['id'].'"><i class="far fa-trash-alt"></i></td>';
            $xhtml.=      '</tr>';
        }
    }

    $categoryXhtml = '<select style="width: 30%" class="form-control" name="id_category">';
    if(!empty($categories)){
      foreach($categories as $value){
          $categoryXhtml.='<option value='.$value['id'].'>'.$value['name'].'</option>';
      }
      $categoryXhtml.= '</select>';
  }
    
?>

    <form method="POST" action="?md=admin&c=postAdmin&m=add" enctype="multipart/form-data">       
        <strong>Title</strong><input type="text" required style="width: 50%" class="form-control" name="title">
        <strong>Image:</strong><input type="file" required style="width: 30%" class="form-control" name="img">  
        <strong>Status:</strong><select style="width: 30%" class="form-control" name="status">
                            <option value="0">Unactive</option>
                            <option value="1">Active</option>
                          </select>   
        <strong>Category: </strong><?php echo $categoryXhtml; ?>                
        <strong>Content</strong><textarea  required style="width: 70%" class="form-control" name="content"></textarea>   
        <button style="margin-top: 10px" type="submit" class="btn btn-primary mb-2">Submit</button>    
    </form>


<table class="table" style="margin-top: 50px">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
      <th scope="col">Image</th>
      <th scope="col">Content</th>
      <th scope="col">Publish Date</th>
      <th scope="col">Status</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $xhtml; ?>
  </tbody>
</table>

