<?php
if(isset($_SESSION['error'])){
    $error = '<div style="background-color:#FF4233; width:50%; padding: 10px; border-radius: 10px;">';
    $error .= '<i style="float:right" class="close far fa-times-circle"></i>';
    foreach($_SESSION['error'] as $value){
       $error.='<p>'.$value.'</p>';
    }
    $error.= '</div>';
    echo $error;
    unset($_SESSION['error']);
  }
  if(isset($_SESSION['notify'])){
   $notify = '<div style="background-color:#33F0FF; width:50%; padding: 10px;border-radius: 10px;">';
   $notify .= '<i style="float:right" class="close far fa-times-circle"></i>';
   $notify .= '<p style="color:white">'.$_SESSION['notify'].'</p>';
   $notify.= '</div>';
   echo $notify;
   unset($_SESSION['notify']);
 }
//echo $item['content'];
     $categoryXhtml = '<select style="width: 30%" class="form-control" name="id_category">';
     if(!empty($categories)){
       foreach($categories as $value){
           if($value['id']==$item['id_category']){
                $categoryXhtml.='<option selected value='.$value['id'].'>'.$value['name'].'</option>';
           }else{
                $categoryXhtml.='<option value='.$value['id'].'>'.$value['name'].'</option>';
           }
       }
    }
       $categoryXhtml.= '</select>';

    if($item['status']==0){
        $statusXhtml = '<option selected value="0">Unactive</option>';
        $statusXhtml .= '<option value="1">active</option>';
    }else{
        $statusXhtml = '<option  value="0">Unactive</option>';
        $statusXhtml .= '<option  selected value="1">active</option>';
    }
?>

<form method="POST" action="?md=admin&c=postAdmin&m=edit" enctype="multipart/form-data">    
        <strong>ID:</strong><input type="text" readonly value="<?php echo $item['id'] ?>" 
        style="width: 50%" class="form-control" name="id">   
        <strong>Title</strong><input type="text" value="<?php echo $item['title'] ?>"
         required style="width: 50%" class="form-control" name="title">
        <strong>Image:</strong><input type="file" required style="width: 30%" class="form-control" name="img"> 
        <image style="width:55px;height:50px" src="/public/img/<?php echo $item['img'];?>"></image> <br>
        <input type="text" hidden name="nameImg" value="<?php echo $item['img'];?>">
        <strong>Status:</strong><select style="width: 30%" class="form-control" name="status">
                            <?php echo $statusXhtml;?>
                            
                            </select>    
        <strong>Category: </strong><?php echo $categoryXhtml; ?>                
        <strong>Content</strong><textarea  required value="<?php echo $item['content'];?>" 
         style="width: 70%" class="form-control" name="content"></textarea>   
        <button style="margin-top: 10px" type="submit" class="btn btn-primary mb-2">Submit</button>    
    </form>
