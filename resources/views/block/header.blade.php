<?php
  use Illuminate\Support\Facades\Auth;
?>
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.html">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto" id="menutop">
          <li class="nav-item">
            <a class="nav-link" href="">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">Sample Post</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
<!--          --><?php
//
//                if(Auth::check()){
//                    echo '<li class="nav-item">
//                            <a class="nav-link" href="contact.html">'. Auth::user()->email .'</a>
//                        </li>';
//                    echo '<li class="nav-item">
//                        <a class="nav-link" href="/logoutt"><span>Logout</span></a>
//                    </li>';
//                    echo '<li class="nav-item">
//                        <a class="nav-link" href="'.route('postIndex').'"><span>Admin</span></a>
//                    </li>';
//                }else
//                    echo '<li class="nav-item">
//                        <a class="nav-link" href="'.route('login').'"><span>Login</span></a>
//                    </li>'
//          ?>

        </ul>
      </div>
    </div>
  </nav>

<!--end nav-->
<header class="masthead" style="background-image: url('<?php echo asset('img/home-bg.jpg') ?>')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>

<script>
        $(document).ready(function() {

        var user = JSON.parse(localStorage.getItem('user'));
            console.log(user);
        if(user){
            $('#menutop').append('<li class="nav-item">\n' +
                '            <a class="nav-link" href="">'+ user.email+'</a>\n' +
                '          </li>');
            $('#menutop').append('<li class="nav-item">\n' +
                '            <a class="nav-link" href="/admin/post">Admin</a>\n' +
                '          </li>');
            $('#menutop').append('<li class="nav-item">\n' +
                '            <a class="nav-link" href="#" onclick="App.logout()">Logout</a>\n' +
                '          </li>');
        }else{
            $('#menutop').append('<li class="nav-item">\n' +
                '            <a class="nav-link" href="/mylogin">Login</a>\n' +
                '          </li>');
        }
    });
</script>
