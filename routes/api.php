<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('auth/register', 'UserController@register');
Route::post('auth/login', 'UserController@login');
Route::post('auth/login',  'UserController@login');

Route::group(['namespace' => 'Api'], function () {
    Route::get('post', 'IndexController@index');
    Route::get('detail/{slug}', 'IndexController@detail');
});

Route::group(['middleware' => 'checktoken'], function () {
    Route::get('user-info', 'UserController@getUserInfo');
    Route::post('comment', 'UserController@getUserInfo');
    Route::get('payload-content', 'UserController@getPayLoad');
});

Route::group(['namespace' => 'Api', 'middleware' => 'checktoken'], function () {
    Route::post('comment', 'CommentController@store');
});
