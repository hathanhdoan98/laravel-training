<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//================ DEFAULT
Route::group(['prefix' => 'frontend','namespace'=>'Frontend'], function () {
    Route::get('/index', [ 'as' =>'index', 'uses' => 'IndexController@index' ]);
    Route::get('/{slug}', [ 'as' =>'detail', 'uses' => 'PostController@detail' ]);
    Route::post('/themcomment', [ 'as' =>'insertcmt', 'uses' => 'PostController@addComment' ]);
    //    Route::get('/addComment/{content}/{comment_date}/{post_id}/{user_id}',
//                [ 'as' =>'addComment', 'uses' => 'PostController@addComment' ]);
});

//================ ADMIN
///
Route::group(['prefix' => 'admin','namespace'=>'Admin','middleware'=>['auth']], function () {
    Route::get('/post', [ 'as' =>'postIndex', 'uses' => 'PostController@index' ]);
    Route::post('/addPost', [ 'as' =>'addPost', 'uses' => 'PostController@add' ]);
    Route::get('/changeStatus/{id}/{status}', [ 'as' =>'changeStatus', 'uses' => 'PostController@changeStatus' ]);
    Route::get('/deletePost/{id}', [ 'as' =>'deletePost', 'uses' => 'PostController@delete' ]);
    Route::get('/editIndexPost/{id}', [ 'as' =>'editIndexPost', 'uses' => 'PostController@editIndex' ]);
    Route::post('/editPost', [ 'as' =>'editPost', 'uses' => 'PostController@edit' ]);

    Route::get('/category', [ 'as' =>'categoryIndex', 'uses' => 'CategoryController@index' ]);
    Route::post('/addCategory', [ 'as' =>'addCategory', 'uses' => 'CategoryController@add' ]);
    Route::get('/changeCategoryStatus/{id}/{status}', [ 'as' =>'changeCategoryStatus', 'uses' => 'CategoryController@changeStatus' ]);
    Route::get('/deleteCategory/{id}', [ 'as' =>'deleteCategory', 'uses' => 'CategoryController@delete' ]);
    Route::get('/editIndexCategory/{id}', [ 'as' =>'editIndexCategory', 'uses' => 'CategoryController@editIndex' ]);
    Route::post('/editCategory', [ 'as' =>'editCategory', 'uses' => 'CategoryController@edit' ]);

});

//===================== API
Route::get('/',function(){
    return view('index.index');
});



Route::group(['prefix' => 'frontendapi'], function () {
    Route::get('/index',function (){
        return view('index.index');
    });
    Route::get('/detail', function (){
        return view('index.postDetail');
    });
    Route::post('/themcomment', [ 'as' =>'insertcmt', 'uses' => 'PostController@addComment' ]);
    //    Route::get('/addComment/{content}/{comment_date}/{post_id}/{user_id}',
//                [ 'as' =>'addComment', 'uses' => 'PostController@addComment' ]);
});

//====================
Auth::routes();

Route::get('/mylogin',function(){
    return view('myLogin.login');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logoutt',function(){
    Auth::logout();

    return redirect()->route('index');
});
